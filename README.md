# README

The goal of the homework project is to implement a queue class

The file queue-homework.cpp contains the test code and a skeleton queue class class IntQueue : public IIntQueue

Your assignment is to implement the functions in IntQueue so the program can run without any asserts You can follow the logic of the program to see how it will be tested Please implement the queue class and not just use a std queue implementation

You can compile and link the program in a gcc docker container

docker run -it --rm -v <path-to-local-source-directory>:/queue-homework gcc /bin/bash cd /queue-homework/ g++ queue-homework.cpp -lpthread -o hw ./hw