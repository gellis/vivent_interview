# the compiler: gcc for C program, define as g++ for C++
CC = g++
CPPFLAGS = -lpthread
TARGET = hw
 
all: $(TARGET)
 
$(TARGET): 
	g++ queue-homework.cpp -lpthread -o $(TARGET) 
 
clean:
	$(RM) $(TARGET)

run:
	./$(TARGET)