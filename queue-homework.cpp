// queue-homework.cpp
//
//
//  running in gcc docker container
//
//  docker run -it --rm -v <path-to-local-source-directory>:/queue-homework gcc /bin/bash
//  cd /queue-homework/
//  g++ queue-homework.cpp  -lpthread -o hw
//  ./hw

#include <stdio.h>
#include <assert.h>
#include <thread>
#include <mutex>
#include <iostream>

//base queue interface
class IIntQueue
{
public:
    // Add an item to the queue
    virtual void Add(int item) = 0;

    // Remove the first item and return it
    virtual int Remove() = 0;

    // return the count of items
    virtual int GetCount() = 0;
};

//
// prototypes
//
void TestQueueProc(void* lpParameter, int id);
void TestQueue(IIntQueue *pQueue);


// TODO
// Add the implementation to IntQueue class so the program can run without
// any asserts

// Create the node structure for linkded list
struct node
{
    int data;
    struct node * next;
};

// Queue class implementation
class IntQueue : public IIntQueue
{
public:
    IntQueue()
    {
        front       = NULL;
        pointer     = NULL;
        newPointer  = NULL;
        rear        = NULL;
        queueSize   = 0;
    }

    virtual void Add(int item)
    {
        mtx.lock();             //<- Start Critical Section
        newPointer          = new node;
        newPointer->data    = item;
        newPointer->next    = NULL;

        if (front == NULL)
        {
            front       = newPointer;
            rear        = newPointer;
            rear->next  = NULL;
        }
        else
        {
            rear->next  = newPointer;
            rear        = newPointer;
            rear->next  = NULL;
        }
        queueSize++;
        mtx.unlock();                   //<- End Critical Section
    };

    virtual int Remove()
    {
        int item;
      
        mtx.lock();                     //<- Start Critical Section

        if (front == NULL)
        {
            std::cout << "Queue Empty" << std::endl;
            return -1;  // TODO: Need a better way to handle this
        }
       
        pointer = front;
        item = pointer->data;
        front = front->next;
        delete(pointer);
        queueSize--;
         mtx.unlock();                   //<- End Critical Section

        return item;
    };

    int GetCount()
    {
        return queueSize;
    };

  
private:

    std::mutex mtx;                 //<- Mutex for critical sections
    int queueSize;                  //<- Queue size
    node *newPointer;               //<- Temporary stoarage of new nodes in linked list
    node *pointer;                  //<- Pointer for current node in linked list
    node *front;                    //<- Front or Head node of linked list
    node *rear;                     //<- Rear or Tail node of linked list
   
};


// Test code below

struct TestQueueData
{
    IIntQueue* pQueue;
    int count;
    bool run;
};

// test queues thread function
void  TestQueueProc( void * lpParameter, int id)
{

    TestQueueData* pData = (TestQueueData*)lpParameter;

    // Wait until thread is told to start
    while (!pData->run)
    {
    }

    // Add count items to the queue.  Each item is the id
    for (int i = 0; i < pData->count; i++)
    {
        pData->pQueue->Add(id);
    }

}

void TestQueue(IIntQueue *pQueue)
{
    // create threads to update the queue

    TestQueueData data;

    data.count = 20000;
    data.pQueue = pQueue;
    data.run = false;

    // Create test threads

    std::thread thread1(TestQueueProc, &data, 1);
    std::thread thread2(TestQueueProc, &data, 2);

    std::this_thread::sleep_for(std::chrono::seconds(1));

    //Notify threads
    data.run = true;

    thread1.join();
    thread2.join();

    int countThread1 = 0;
    int countThread2 = 0;

    // check the data
    printf("dump:  count=%d\n", pQueue->GetCount());

    assert(data.count * 2 == pQueue->GetCount());

    while (pQueue->GetCount() > 0)
    {
        int item = pQueue->Remove();

        if (item == 1)
        {
            countThread1++;
        }
        else if (item == 2)
        {
            countThread2++;
        }
        else
        {
            assert(0);
        }
    }

    printf("dump:  count=%d  CountID1=%d  CountID2=%d\n", pQueue->GetCount(), countThread1, countThread2);

    assert(countThread1 == data.count);
    assert(countThread2 == data.count);
}


int main(int argc, char** argv)
{
    // Test the queue implementation
    IntQueue queue1;
    TestQueue(&queue1);

    return 0;
}

